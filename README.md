<img style="float:right;" id="hp" src="./doc/img/Logo.png"/>

# Open Machines Services Coordinator

Micronaut "micro"-service running the BPMN engine.

## Runtime Context


```plantuml
@startuml
!theme spacelab 



partition **ServicesCoordinator** #lightgreen {
    
    ServicesCoordinatorRenderingEngine --> ServicesCoordinatorController
    ServicesCoordinatorRestAPI --> ServicesCoordinatorController
    ServicesCoordinatorController --> BpmnEngine

}

partition AnotherBusinessModule {
    AnotherBusinessWebComponents --> [https] AnotherBusinessController
    AnotherBusinessController --> AnotherBusiness-µStreamPersistence
}
partition SomeBusinessModule {
    SomeBusinessWebComponents --> [https] SomeBusinessController
    SomeBusinessController -u-> SomeBusiness-µStreamPersistence
}
partition TasklistModule {
    TasklistWebComponents --> [https] TasklistController
    TasklistController --> Tasklist-µStreamPersistence
}

partition SomeExternalService {
    SomeExternalServiceRestAPI -d-> ServicesCoordinatorRestAPI
}

partition OpenSocial {
    SPALP -up-> OpenSocialUI
    OpenSocialUI --> Drupal
}



SPALP --> ServicesCoordinatorRenderingEngine
SPALP --> AnotherBusinessWebComponents
SPALP --> TasklistWebComponents
SPALP --> SomeBusinessWebComponents

AnotherBusinessController -down-> ServicesCoordinatorRestAPI
SomeBusinessController -down-> ServicesCoordinatorRestAPI
TasklistController -down-> ServicesCoordinatorRestAPI
SomeBusinessController -d-> SomeExternalServiceRestAPI
BpmnEngine -u-> SomeExternalServiceRestAPI

@enduml

```

The basic approach is, that each microservice renders it's own content into a canvas within the Open Social UI. The details of the frontend integration are left to the service. Good fits might be

* [Web Components](https://developer.mozilla.org/de/docs/Web/Web_Components)
* [HOTWire](https://hotwired.dev/)
* Rendering Engines like [Apache Freemaker](https://freemarker.apache.org/), [Velocity](https://velocity.apache.org/) or maybe even JSP
* Small single page WebApps if necessary, ideally in a new Window 

The Services Coordinator has only an administrative frontend for status information. Hence we use Velocity to make our life easy.

```plantuml
@startsalt
skinparam handwritten true
skinparam backgroundColor #EEEBDC
scale 2.2
{^"Opensocial Page"
 
{<color:blue><size:18> open<&code>machines | [<&home>Home] |[Explore] |<size:21><&magnifying-glass> }| <size:21> <&plus><&team><&bell><&cog><&person>
---------|------
{+ <color:gray>(opensoical included)
    {"Say something to the Community"}
    {[<&plus>Add image]|[<&person>Community]|[Post]} 
}
{S <color:gray>**Some Business Module** 
----------
Design | "My great design"
Maturity | ^Concept Stage^
Approval Type| ^Electrical Safety^
.| [X] Certification requested
etc...
[Cancel]|[Release for Approval]
}{S 
<color:gray>**Tasklist**
<color:gray>**Module**
----------

<&move><&menu><&share-boxed> Approve docu...
<&move><&menu><&share-boxed> Request certif...
<&move><&menu><&share-boxed> Fix design iss...
[Add Task]
[Start Process]
}
{S <back:orange>**Services Coordinator Module**

----------
**151** active processes by **144** users | [show statistics]
12 stuck processes | [terminate]

[Open Admin Console]
}{^ 
<color:gray>**Another Business ** 
<color:gray>**Module**
----------
<size:84><&clock>
[Start] 
[Pause]
}

{+ <color:gray>(opensoical included)
    {Timeline Content ...}
    <&person> helmut posted
    <size:8>1 week ago community
    <size:199><color:green><&compass>
    <&heart> 12 likes
    "Write a comment" | [Comment]
    
}{+ <color:gray>(opensoical included)
    {Upcoming events}
    .
    **Guideline Team Meeting**
    <color:gray>30 Oct 21 19:00 - 19:45
.
    [show all events]
    
}

}
@endsalt

```

---

## Micronaut 3.0.3 Documentation

- [User Guide](https://docs.micronaut.io/3.0.3/guide/index.html)
- [API Reference](https://docs.micronaut.io/3.0.3/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/3.0.3/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)

## Feature testcontainers documentation

- [https://www.testcontainers.org/](https://www.testcontainers.org/)

## Feature http-client documentation

- [Micronaut HTTP Client documentation](https://docs.micronaut.io/latest/guide/index.html#httpClient)

## Feature views-velocity documentation

- [Micronaut Velocity views documentation](https://micronaut-projects.github.io/micronaut-views/latest/guide/index.html#velocity)

- [https://velocity.apache.org](https://velocity.apache.org)

## Feature lombok documentation

- [Micronaut Project Lombok documentation](https://docs.micronaut.io/latest/guide/index.html#lombok)

- [https://projectlombok.org/features/all](https://projectlombok.org/features/all)

## Feature ServicesCoordinator documentation

- [https://github.com/ServicesCoordinator-community-hub/micronaut-ServicesCoordinator-bpm](https://github.com/ServicesCoordinator-community-hub/micronaut-ServicesCoordinator-bpm)

## Feature config-kubernetes documentation

- [Micronaut Kubernetes Distributed Configuration documentation](https://micronaut-projects.github.io/micronaut-kubernetes/latest/guide/index.html)

## Feature kubernetes documentation

- [Micronaut Kubernetes Support documentation](https://micronaut-projects.github.io/micronaut-kubernetes/latest/guide/index.html)

- [https://kubernetes.io/docs/home/](https://kubernetes.io/docs/home/)

## Feature management documentation

- [Micronaut Management documentation](https://docs.micronaut.io/latest/guide/index.html#management)

## Feature jdbc-hikari documentation

- [Micronaut Hikari JDBC Connection Pool documentation](https://micronaut-projects.github.io/micronaut-sql/latest/guide/index.html#jdbc)

## Feature assertj documentation

- [https://assertj.github.io/doc/](https://assertj.github.io/doc/)

